require "application_system_test_case"

class EventsTest < ApplicationSystemTestCase
  setup do
    @event = events(:one)
  end

  test "visiting the index" do
    visit events_url
    assert_selector "h1", text: "Events"
  end

  test "creating a Event" do
    visit events_url
    click_on "New Event"

    fill_in "Age max", with: @event.age_max
    fill_in "Age min", with: @event.age_min
    fill_in "Competition", with: @event.competition_id
    fill_in "Day", with: @event.day
    fill_in "Discipline", with: @event.discipline_id
    fill_in "Gender", with: @event.gender
    fill_in "Pos", with: @event.pos
    fill_in "Seeded at", with: @event.seeded_at
    fill_in "Stage", with: @event.stage
    click_on "Create Event"

    assert_text "Event was successfully created"
    click_on "Back"
  end

  test "updating a Event" do
    visit events_url
    click_on "Edit", match: :first

    fill_in "Age max", with: @event.age_max
    fill_in "Age min", with: @event.age_min
    fill_in "Competition", with: @event.competition_id
    fill_in "Day", with: @event.day
    fill_in "Discipline", with: @event.discipline_id
    fill_in "Gender", with: @event.gender
    fill_in "Pos", with: @event.pos
    fill_in "Seeded at", with: @event.seeded_at
    fill_in "Stage", with: @event.stage
    click_on "Update Event"

    assert_text "Event was successfully updated"
    click_on "Back"
  end

  test "destroying a Event" do
    visit events_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Event was successfully destroyed"
  end
end
