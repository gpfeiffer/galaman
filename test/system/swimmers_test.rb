require "application_system_test_case"

class SwimmersTest < ApplicationSystemTestCase
  setup do
    @swimmer = swimmers(:one)
  end

  test "visiting the index" do
    visit swimmers_url
    assert_selector "h1", text: "Swimmers"
  end

  test "creating a Swimmer" do
    visit swimmers_url
    click_on "New Swimmer"

    fill_in "Birthday", with: @swimmer.birthday
    fill_in "Club", with: @swimmer.club_id
    fill_in "Comment", with: @swimmer.comment
    fill_in "First", with: @swimmer.first
    fill_in "Gender", with: @swimmer.gender
    fill_in "Last", with: @swimmer.last
    fill_in "Number", with: @swimmer.number
    fill_in "User", with: @swimmer.user_id
    click_on "Create Swimmer"

    assert_text "Swimmer was successfully created"
    click_on "Back"
  end

  test "updating a Swimmer" do
    visit swimmers_url
    click_on "Edit", match: :first

    fill_in "Birthday", with: @swimmer.birthday
    fill_in "Club", with: @swimmer.club_id
    fill_in "Comment", with: @swimmer.comment
    fill_in "First", with: @swimmer.first
    fill_in "Gender", with: @swimmer.gender
    fill_in "Last", with: @swimmer.last
    fill_in "Number", with: @swimmer.number
    fill_in "User", with: @swimmer.user_id
    click_on "Update Swimmer"

    assert_text "Swimmer was successfully updated"
    click_on "Back"
  end

  test "destroying a Swimmer" do
    visit swimmers_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Swimmer was successfully destroyed"
  end
end
