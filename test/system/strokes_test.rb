require "application_system_test_case"

class StrokesTest < ApplicationSystemTestCase
  setup do
    @stroke = strokes(:one)
  end

  test "visiting the index" do
    visit strokes_url
    assert_selector "h1", text: "Strokes"
  end

  test "creating a Stroke" do
    visit strokes_url
    click_on "New Stroke"

    fill_in "Code", with: @stroke.code
    fill_in "Name", with: @stroke.name
    fill_in "Short", with: @stroke.short
    click_on "Create Stroke"

    assert_text "Stroke was successfully created"
    click_on "Back"
  end

  test "updating a Stroke" do
    visit strokes_url
    click_on "Edit", match: :first

    fill_in "Code", with: @stroke.code
    fill_in "Name", with: @stroke.name
    fill_in "Short", with: @stroke.short
    click_on "Update Stroke"

    assert_text "Stroke was successfully updated"
    click_on "Back"
  end

  test "destroying a Stroke" do
    visit strokes_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Stroke was successfully destroyed"
  end
end
