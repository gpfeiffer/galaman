require "application_system_test_case"

class QualificationTimesTest < ApplicationSystemTestCase
  setup do
    @qualification_time = qualification_times(:one)
  end

  test "visiting the index" do
    visit qualification_times_url
    assert_selector "h1", text: "Qualification Times"
  end

  test "creating a Qualification time" do
    visit qualification_times_url
    click_on "New Qualification Time"

    fill_in "Age max", with: @qualification_time.age_max
    fill_in "Age min", with: @qualification_time.age_min
    fill_in "Discipline", with: @qualification_time.discipline_id
    fill_in "Gender", with: @qualification_time.gender
    fill_in "Qualification", with: @qualification_time.qualification_id
    fill_in "Time", with: @qualification_time.time
    click_on "Create Qualification time"

    assert_text "Qualification time was successfully created"
    click_on "Back"
  end

  test "updating a Qualification time" do
    visit qualification_times_url
    click_on "Edit", match: :first

    fill_in "Age max", with: @qualification_time.age_max
    fill_in "Age min", with: @qualification_time.age_min
    fill_in "Discipline", with: @qualification_time.discipline_id
    fill_in "Gender", with: @qualification_time.gender
    fill_in "Qualification", with: @qualification_time.qualification_id
    fill_in "Time", with: @qualification_time.time
    click_on "Update Qualification time"

    assert_text "Qualification time was successfully updated"
    click_on "Back"
  end

  test "destroying a Qualification time" do
    visit qualification_times_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Qualification time was successfully destroyed"
  end
end
