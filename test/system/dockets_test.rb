require "application_system_test_case"

class DocketsTest < ApplicationSystemTestCase
  setup do
    @docket = dockets(:one)
  end

  test "visiting the index" do
    visit dockets_url
    assert_selector "h1", text: "Dockets"
  end

  test "creating a Docket" do
    visit dockets_url
    click_on "New Docket"

    fill_in "Age", with: @docket.age
    fill_in "Invitation", with: @docket.invitation_id
    fill_in "Swimmer", with: @docket.swimmer_id
    click_on "Create Docket"

    assert_text "Docket was successfully created"
    click_on "Back"
  end

  test "updating a Docket" do
    visit dockets_url
    click_on "Edit", match: :first

    fill_in "Age", with: @docket.age
    fill_in "Invitation", with: @docket.invitation_id
    fill_in "Swimmer", with: @docket.swimmer_id
    click_on "Update Docket"

    assert_text "Docket was successfully updated"
    click_on "Back"
  end

  test "destroying a Docket" do
    visit dockets_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Docket was successfully destroyed"
  end
end
