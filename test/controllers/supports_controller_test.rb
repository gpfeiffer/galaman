require 'test_helper'

class SupportsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    sign_in users(:admin)
    @support = supports(:one)
  end

  test "should get index" do
    get supports_url
    assert_response :success
  end

  test "should get new" do
    get new_support_url(user_id: @support.user)
    assert_response :success
  end

  test "should create support" do
    assert_difference('Support.count') do
      post supports_url, params: { support: { swimmer_id: @support.swimmer_id, user_id: @support.user_id } }
    end

    assert_redirected_to user_url(@support.user)
  end

  test "should show support" do
    get support_url(@support)
    assert_response :success
  end

  test "should get edit" do
    get edit_support_url(@support)
    assert_response :success
  end

  test "should update support" do
    patch support_url(@support), params: { support: { swimmer_id: @support.swimmer_id, user_id: @support.user_id } }
    assert_redirected_to user_url(@support.user)
  end

  test "should destroy support" do
    assert_difference('Support.count', -1) do
      delete support_url(@support)
    end

    assert_redirected_to user_url(@support.user)
  end
end
