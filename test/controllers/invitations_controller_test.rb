require 'test_helper'

class InvitationsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    sign_in users(:admin)
    @invitation = invitations(:one)
    @club = clubs(:two)
    @competition = competitions(:one)
  end

  test "should get index" do
    get invitations_url(club_id: @club)
    assert_response :success
  end

  test "should get new" do
    get new_invitation_url(club_id: @invitation.club_id, competition_id: @invitation.competition_id)
    assert_response :success
  end

  test "should create invitation" do
    assert_difference('Invitation.count') do
      post invitations_url, params: { invitation: { club_id: @invitation.club_id, competition_id: @invitation.competition_id } }
    end

    assert_redirected_to competition_url(@invitation.competition)
  end

  test "should show invitation" do
    get invitation_url(@invitation)
    assert_response :success
  end

  test "should get edit" do
    get edit_invitation_url(@invitation)
    assert_response :success
  end

  test "should update invitation" do
    patch invitation_url(@invitation), params: { invitation: { club_id: @invitation.club_id, competition_id: @invitation.competition_id, swimmer_ids: [] } }
    assert_redirected_to invitation_url(@invitation)
  end

  test "should destroy invitation" do
    assert_difference('Invitation.count', -1) do
      delete invitation_url(@invitation)
    end

    assert_redirected_to competition_url(@invitation.competition)
  end
end
