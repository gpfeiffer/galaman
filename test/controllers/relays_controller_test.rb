require 'test_helper'

class RelaysControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    sign_in users(:admin)
    @relay = relays(:one)
  end

  test "should get index" do
    get relays_url
    assert_response :success
  end

  test "should get new" do
    get new_relay_url(invitation_id: @relay.invitation)
    assert_response :success
  end

  test "should create relay" do
    assert_difference('Relay.count') do
      post relays_url, params: { relay: { age_max: @relay.age_max, age_min: @relay.age_min, gender: @relay.gender, invitation_id: @relay.invitation_id, name: @relay.name } }
    end

    assert_redirected_to invitation_url(@relay.invitation)
  end

  test "should show relay" do
    get relay_url(@relay)
    assert_response :success
  end

  test "should get edit" do
    get edit_relay_url(@relay)
    assert_response :success
  end

  test "should update relay" do
    patch relay_url(@relay), params: { relay: { age_max: @relay.age_max, age_min: @relay.age_min, gender: @relay.gender, invitation_id: @relay.invitation_id, name: "New Relay" } }
    assert_redirected_to invitation_url(@relay.invitation)
  end

  test "should destroy relay" do
    assert_difference('Relay.count', -1) do
      delete relay_url(@relay)
    end

    assert_redirected_to invitation_url(@relay.invitation)
  end
end
