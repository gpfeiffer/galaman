require 'test_helper'

class EventsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    sign_in users(:admin)
    @event = events(:one)
  end

  test "should get index" do
    get events_url
    assert_response :success
  end

  test "should get new" do
    get new_event_url(competition_id: @event.competition)
    assert_response :success
  end

  test "should create event" do
    assert_difference('Event.count') do
      post events_url, params: { event: { age_max: @event.age_max, age_min: @event.age_min, competition_id: @event.competition_id, day: @event.day, discipline_id: @event.discipline_id, gender: @event.gender, pos: @event.pos, seeded_at: @event.seeded_at, stage: @event.stage, distance: @event.discipline.distance, course: @event.discipline.course, stroke: @event.discipline.stroke, mode: @event.discipline.mode } }
    end

    assert_redirected_to event_url(Event.last)
  end

  test "should show event" do
    get event_url(@event)
    assert_response :success
  end

  test "should get edit" do
    get edit_event_url(@event)
    assert_response :success
  end

  test "should update event" do
    patch event_url(@event), params: { event: { age_max: @event.age_max, age_min: @event.age_min, competition_id: @event.competition_id, day: @event.day, discipline_id: @event.discipline_id, gender: @event.gender, pos: @event.pos, seeded_at: @event.seeded_at, stage: @event.stage, distance: @event.discipline.distance, course: @event.discipline.course, stroke: @event.discipline.stroke, mode: @event.discipline.mode } }
    assert_redirected_to competition_url(@event.competition)
  end

  test "should destroy event" do
    assert_difference('Event.count', -1) do
      delete event_url(@event)
    end

    assert_redirected_to competition_url(@event.competition)
  end
end
