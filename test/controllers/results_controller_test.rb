require 'test_helper'

class ResultsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    sign_in users(:admin)
    @result = results(:one)
  end

  test "should get index" do
    get results_url
    assert_response :success
  end

  test "should get new" do
    get new_result_url(entry_id: @result.entry)
    assert_response :success
  end

  test "should create result" do
    assert_difference('Result.count') do
      post results_url, params: { result: { comment: @result.comment, entry_id: @result.entry_id, heat: @result.heat, lane: @result.lane, place: @result.place, stage: @result.stage, time: @result.time } }
    end

    assert_redirected_to event_url(@result.event)
  end

  test "should show result" do
    get result_url(@result)
    assert_response :success
  end

  test "should get edit" do
    get edit_result_url(@result)
    assert_response :success
  end

  test "should update result" do
    patch result_url(@result), params: { result: { comment: @result.comment, entry_id: @result.entry_id, heat: @result.heat, lane: @result.lane, place: @result.place, stage: @result.stage, time: @result.time } }
    assert_redirected_to event_url(@result.event)
  end

  test "should destroy result" do
    assert_difference('Result.count', -1) do
      delete result_url(@result)
    end

    assert_redirected_to event_url(@result.event)
  end
end
