require 'test_helper'

class EntriesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    sign_in users(:admin)
    @entry = entries(:one)
  end

  test "should get index" do
    get entries_url
    assert_response :success
  end

  test "should get new" do
    get new_entry_url(event_id: @entry.event, subject_type: @entry.subject_type, subject_id: @entry.subject_id)
    assert_response :success
  end

  test "should create entry" do
    assert_difference('Entry.count') do
      post entries_url, params: { entry: { event_id: @entry.event_id, group: @entry.group, heat: @entry.heat, lane: @entry.lane, stage: @entry.stage, subject_id: @entry.subject_id, subject_type: @entry.subject_type, time: @entry.time } }
    end

    assert_redirected_to invitation_url(@entry.invitation)
  end

  test "should show entry" do
    get entry_url(@entry)
    assert_response :success
  end

  test "should get edit" do
    get edit_entry_url(@entry)
    assert_response :success
  end

  test "should update entry" do
    patch entry_url(@entry), params: { entry: { event_id: @entry.event_id, group: @entry.group, heat: @entry.heat, lane: @entry.lane, stage: @entry.stage, subject_id: @entry.subject_id, subject_type: @entry.subject_type, time: @entry.time } }
    assert_redirected_to invitation_url(@entry.invitation)
  end

  test "should destroy entry" do
    assert_difference('Entry.count', -1) do
      delete entry_url(@entry)
    end

    assert_redirected_to invitation_url(@entry.invitation)
  end
end
