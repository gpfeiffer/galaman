require 'test_helper'

class QualificationTimesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    sign_in users(:admin)
    @qualification_time = qualification_times(:one)
    @discipline = disciplines(:one)
  end

  test "should get index" do
    get qualification_times_url
    assert_response :success
  end

  test "should get new" do
    get new_qualification_time_url(qualification_id: @qualification_time.qualification)
    assert_response :success
  end

  test "should create qualification_time" do
    assert_difference('QualificationTime.count') do
      post qualification_times_url, params: { qualification_time: { age_max: @qualification_time.age_max, age_min: @qualification_time.age_min, discipline_id: @qualification_time.discipline_id, gender: @qualification_time.gender, qualification_id: @qualification_time.qualification_id, time: @qualification_time.time }, time: { mins: 1, secs: 1, cens: 1 }, discipline: { distance: @discipline.distance, course: @discipline.course, stroke: @discipline.stroke, mode: @discipline.mode } }
    end

    assert_redirected_to qualification_url(@qualification_time.qualification)
  end

  test "should show qualification_time" do
    get qualification_time_url(@qualification_time)
    assert_response :success
  end

  test "should get edit" do
    get edit_qualification_time_url(@qualification_time)
    assert_response :success
  end

  test "should update qualification_time" do
    patch qualification_time_url(@qualification_time), params: { qualification_time: { age_max: @qualification_time.age_max, age_min: @qualification_time.age_min, discipline_id: @qualification_time.discipline_id, gender: @qualification_time.gender, qualification_id: @qualification_time.qualification_id, time: @qualification_time.time } }
    assert_redirected_to qualification_url(@qualification_time.qualification)
  end

  test "should destroy qualification_time" do
    assert_difference('QualificationTime.count', -1) do
      delete qualification_time_url(@qualification_time)
    end

    assert_redirected_to qualification_url(@qualification_time.qualification)
  end
end
