require 'test_helper'

class QualificationsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    sign_in users(:admin)
    @qualification = qualifications(:one)
  end

  test "should get index" do
    get qualifications_url
    assert_response :success
  end

  test "should get new" do
    get new_qualification_url
    assert_response :success
  end

  test "should create qualification" do
    assert_difference('Qualification.count') do
      post qualifications_url, params: { qualification: { description: @qualification.description, name: "New Name", short: @qualification.short, source: @qualification.source, source_date: @qualification.source_date, source_url: @qualification.source_url } }
    end

    assert_redirected_to qualification_url(Qualification.last)
  end

  test "should show qualification" do
    get qualification_url(@qualification)
    assert_response :success
  end

  test "should get edit" do
    get edit_qualification_url(@qualification)
    assert_response :success
  end

  test "should update qualification" do
    patch qualification_url(@qualification), params: { qualification: { description: @qualification.description, name: "New Name", short: @qualification.short, source: @qualification.source, source_date: @qualification.source_date, source_url: @qualification.source_url } }
    assert_redirected_to qualification_url(@qualification)
  end

  test "should destroy qualification" do
    assert_difference('Qualification.count', -1) do
      delete qualification_url(@qualification)
    end

    assert_redirected_to qualifications_url
  end
end
