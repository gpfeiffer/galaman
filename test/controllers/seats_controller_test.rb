require 'test_helper'

class SeatsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    sign_in users(:admin)
    @seat = seats(:one)
  end

  test "should get index" do
    get seats_url
    assert_response :success
  end

  test "should get new" do
    get new_seat_url(relay_id: @seat.relay, docket_id: @seat.docket)
    assert_response :success
  end

  test "should create seat" do
    assert_difference('Seat.count') do
      post seats_url, params: { seat: { docket_id: @seat.docket_id, pos: @seat.pos, relay_id: @seat.relay_id } }
    end

    assert_redirected_to invitation_url(@seat.invitation)
  end

  test "should show seat" do
    get seat_url(@seat)
    assert_response :success
  end

  test "should get edit" do
    get edit_seat_url(@seat)
    assert_response :success
  end

  test "should update seat" do
    patch seat_url(@seat), params: { seat: { docket_id: @seat.docket_id, pos: @seat.pos, relay_id: @seat.relay_id } }
    assert_redirected_to invitation_url(@seat.invitation)
  end

  test "should destroy seat" do
    assert_difference('Seat.count', -1) do
      delete seat_url(@seat)
    end

    assert_redirected_to invitation_url(@seat.invitation)
  end
end
