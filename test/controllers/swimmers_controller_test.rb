require 'test_helper'

class SwimmersControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    sign_in users(:admin)
    @swimmer = swimmers(:one)
  end

  test "should get index" do
    get swimmers_url
    assert_response :success
  end

  test "should get new" do
    get new_swimmer_url(club_id: @swimmer.club)
    assert_response :success
  end

  test "should create swimmer" do
    assert_difference('Swimmer.count') do
      post swimmers_url, params: { swimmer: { birthday: @swimmer.birthday, club_id: @swimmer.club_id, comment: @swimmer.comment, first: "first", gender: @swimmer.gender, last: "last", number: 123, user_id: @swimmer.user_id } }
    end

    assert_redirected_to swimmer_url(Swimmer.last)
  end

  test "should show swimmer" do
    get swimmer_url(@swimmer)
    assert_response :success
  end

  test "should get edit" do
    get edit_swimmer_url(@swimmer)
    assert_response :success
  end

  test "should update swimmer" do
    patch swimmer_url(@swimmer), params: { swimmer: { birthday: @swimmer.birthday, club_id: @swimmer.club_id, comment: "no comment", first: @swimmer.first, gender: @swimmer.gender, last: @swimmer.last, number: @swimmer.number, user_id: @swimmer.user_id } }
    assert_redirected_to swimmer_url(@swimmer)
  end

  test "should destroy swimmer" do
    assert_difference('Swimmer.count', -1) do
      delete swimmer_url(@swimmer)
    end

    assert_redirected_to club_url(@swimmer.club)
  end
end
