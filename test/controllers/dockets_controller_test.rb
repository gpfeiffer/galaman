require 'test_helper'

class DocketsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    sign_in users(:admin)
    @docket = dockets(:one)
  end

  test "should get index" do
    get dockets_url(invitation_id: @docket.invitation)
    assert_response :success
  end

  test "should get new" do
    get new_docket_url(competition_id: @docket.competition, swimmer_id: @docket.swimmer)
    assert_response :success
  end

  test "should create docket" do
    assert_difference('Docket.count') do
      post dockets_url, params: { docket: { age: @docket.age, invitation_id: @docket.invitation_id, swimmer_id: @docket.swimmer_id } }
    end

    assert_redirected_to docket_url(Docket.last)
  end

  test "should show docket" do
    get docket_url(@docket)
    assert_response :success
  end

  test "should get edit" do
    get edit_docket_url(@docket)
    assert_response :success
  end

  test "should update docket" do
    patch docket_url(@docket), params: { docket: { age: @docket.age, invitation_id: @docket.invitation_id, swimmer_id: @docket.swimmer_id } }
    assert_redirected_to docket_url(@docket)
  end

  test "should destroy docket" do
    assert_difference('Docket.count', -1) do
      delete docket_url(@docket)
    end

    assert_redirected_to invitation_url(@docket.invitation)
  end
end
