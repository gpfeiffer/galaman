require 'test_helper'

class AimsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    sign_in users(:admin)
    @aim = aims(:one)
    @swimmer = swimmers(:one)
  end

  test "should get index" do
    get aims_url
    assert_response :success
  end

  test "should get new" do
    get new_aim_url(swimmer_id: @swimmer)
    assert_response :success
  end

  test "should create aim" do
    assert_difference('Aim.count') do
      post aims_url, params: { aim: { date: @aim.date, qualification_id: @aim.qualification_id, swimmer_id: @aim.swimmer_id } }
    end

    assert_redirected_to aim_url(Aim.last)
  end

  test "should show aim" do
    get aim_url(@aim)
    assert_response :success
  end

  test "should get edit" do
    get edit_aim_url(@aim)
    assert_response :success
  end

  test "should update aim" do
    patch aim_url(@aim), params: { aim: { date: @aim.date, qualification_id: @aim.qualification_id, swimmer_id: @aim.swimmer_id } }
    assert_redirected_to aim_url(@aim)
  end

  test "should destroy aim" do
    assert_difference('Aim.count', -1) do
      delete aim_url(@aim)
    end

    assert_redirected_to @aim.swimmer
  end
end
