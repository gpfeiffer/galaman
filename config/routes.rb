Rails.application.routes.draw do
  resources :splits
  resources :results
  resources :entries
  resources :aims
  resources :seats
  resources :dockets
  resources :relays

  resources :events do
    member do
      post 'list'
      post 'seed'
    end
  end

  resources :invitations
  resources :supports
  resources :qualification_times
  resources :swimmers
  resources :standards
  resources :competitions
  resources :qualifications
  resources :disciplines
  resources :clubs
  resources :strokes
  resources :assignments
  resources :roles

  devise_for :users
  scope :admin do
    resources :users
  end

  get 'heats' => 'heats#index'
  get 'lanes' => 'lanes#index'

  get 'home/index'
  root to: 'home#index'
end
