class CompetitionsController < ApplicationController
  before_action :set_competition, only: [:show, :edit, :update, :destroy]

  # GET /competitions
  # GET /competitions.json
  def index
    authorize! :index, Competition
    @competitions = Competition.all
    @competitions_by_season = @competitions.group_by(&:season)
  end

  # GET /competitions/1
  # GET /competitions/1.json
  def show
    authorize! :show, @competition
    @invitations = @competition.invitations.includes(:club, :dockets, :relays, :entries)
    @events = @competition.events.includes(:entries, :discipline)
    respond_to do |format|
      format.html # show.html.erb
      format.tex
    end
  end

  # GET /competitions/new
  def new
    @competition = Competition.new(length: 1)
    authorize! :new, @competition
  end

  # GET /competitions/1/edit
  def edit
    authorize! :edit, @competition
  end

  # POST /competitions
  # POST /competitions.json
  def create
    @competition = Competition.new(competition_params)
    authorize! :create, @competition

    respond_to do |format|
      if @competition.save
        format.html { redirect_to @competition, notice: 'Competition was successfully created.' }
        format.json { render :show, status: :created, location: @competition }
      else
        format.html { render :new }
        format.json { render json: @competition.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /competitions/1
  # PATCH/PUT /competitions/1.json
  def update
    authorize! :update, @competition
    respond_to do |format|
      if @competition.update(competition_params)
        format.html { redirect_to @competition, notice: 'Competition was successfully updated.' }
        format.json { render :show, status: :ok, location: @competition }
      else
        format.html { render :edit }
        format.json { render json: @competition.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /competitions/1
  # DELETE /competitions/1.json
  def destroy
    authorize! :destroy, @competition
    @competition.destroy
    respond_to do |format|
      format.html { redirect_to competitions_url, notice: 'Competition was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_competition
      @competition = Competition.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def competition_params
      params.require(:competition).permit(:name, :location, :course, :age_up, :date, :length, :source, :source_url, :source_date)
    end
end
