class DisciplinesController < ApplicationController
  skip_before_action :authenticate_user!, only: [:index, :show]
  before_action :set_discipline, only: [:edit, :update, :destroy]

  # GET /disciplines
  # GET /disciplines.json
  def index
    authorize! :index, Discipline
    @disciplines = Discipline.includes(:events, :qualification_times)
    if params[:club_id]
      @club = Club.find(params[:club_id])
    end
  end

  # GET /disciplines/1
  # GET /disciplines/1.json
  def show
    @discipline = Discipline.includes(
      { qualification_times: :qualification },
      :competitions,
      { results: { entry: { subject: [:swimmer, { invitation: :club }] } } }
    ).find(params[:id])
    authorize! :show, @discipline
    @qts_by_qualification = @discipline.qualification_times.group_by(&:qualification)

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @discipline }
    end
  end

  # GET /disciplines/new
  def new
    @discipline = Discipline.new
    authorize! :new, @discipline
  end

  # GET /disciplines/1/edit
  def edit
    authorize! :edit, @discipline
  end

  # POST /disciplines
  # POST /disciplines.json
  def create
    @discipline = Discipline.new(discipline_params)
    authorize! :create, @discipline

    respond_to do |format|
      if @discipline.save
        format.html { redirect_to @discipline, notice: 'Discipline was successfully created.' }
        format.json { render :show, status: :created, location: @discipline }
      else
        format.html { render :new }
        format.json { render json: @discipline.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /disciplines/1
  # PATCH/PUT /disciplines/1.json
  def update
    authorize! :update, @discipline
    respond_to do |format|
      if @discipline.update(discipline_params)
        format.html { redirect_to @discipline, notice: 'Discipline was successfully updated.' }
        format.json { render :show, status: :ok, location: @discipline }
      else
        format.html { render :edit }
        format.json { render json: @discipline.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /disciplines/1
  # DELETE /disciplines/1.json
  def destroy
    authorize! :destroy, @discipline
    @discipline.destroy
    respond_to do |format|
      format.html { redirect_to disciplines_url, notice: 'Discipline was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_discipline
      @discipline = Discipline.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def discipline_params
      params.require(:discipline).permit(:gender, :distance, :course, :stroke, :mode, :differential)
    end
end
