class StrokesController < ApplicationController
  before_action :set_stroke, only: [:show, :edit, :update, :destroy]

  # GET /strokes
  # GET /strokes.json
  def index
    authorize! @index, Stroke
    @strokes = Stroke.all
    if params[:club_id]
      @club = Club.find(params[:club_id])
    end
  end

  # GET /strokes/1
  # GET /strokes/1.json
  def show
    authorize! :show, @stroke
    if params[:club_id]
      @club = Club.find(params[:club_id])
      @results = @club.swimmers.map(&:results).flatten.select {
        |x| x.time && x.time > 0 && x.stroke == @stroke.name
      }
    end
  end

  # GET /strokes/new
  def new
    @stroke = Stroke.new
    authorize! :new, @stroke
  end

  # GET /strokes/1/edit
  def edit
    authorize! :edit, @stroke
  end

  # POST /strokes
  # POST /strokes.json
  def create
    @stroke = Stroke.new(stroke_params)
    authorize! :create, @stroke

    respond_to do |format|
      if @stroke.save
        format.html { redirect_to @stroke, notice: 'Stroke was successfully created.' }
        format.json { render :show, status: :created, location: @stroke }
      else
        format.html { render :new }
        format.json { render json: @stroke.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /strokes/1
  # PATCH/PUT /strokes/1.json
  def update
    authorize! :update, @stroke
    respond_to do |format|
      if @stroke.update(stroke_params)
        format.html { redirect_to @stroke, notice: 'Stroke was successfully updated.' }
        format.json { render :show, status: :ok, location: @stroke }
      else
        format.html { render :edit }
        format.json { render json: @stroke.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /strokes/1
  # DELETE /strokes/1.json
  def destroy
    authorize! :delete, @stroke
    @stroke.destroy
    respond_to do |format|
      format.html { redirect_to strokes_url, notice: 'Stroke was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_stroke
      @stroke = Stroke.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def stroke_params
      params.require(:stroke).permit(:name, :short, :code)
    end
end
