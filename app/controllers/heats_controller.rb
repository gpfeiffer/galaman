class HeatsController < ApplicationController

  # GET /heats
  # GET /heats.json
  def index
    authorize! :index, Event
    if params[:event_id]
      @event = Event.find(params[:event_id])
      @heats = @event.heats
    elsif params[:competition_id]
      @competition = Competition.find(params[:competition_id])
    end
  end

end
