class SeatsController < ApplicationController
  before_action :set_seat, only: [:show, :edit, :update, :destroy]

  # GET /seats
  # GET /seats.json
  def index
    authorize! :index, Seat
    @seats = Seat.all
  end

  # GET /seats/1
  # GET /seats/1.json
  def show
    authorize! :show, @seat
  end

  # GET /seats/new
  def new
    @seat = Seat.new
    authorize! :new, @seat
    @seat.relay = Relay.find(params[:relay_id])
    @seat.docket = Docket.find(params[:docket_id])
  end

  # GET /seats/1/edit
  def edit
    authorize! :edit, @seat
  end

  # POST /seats
  # POST /seats.json
  def create
    @seat = Seat.new(seat_params)
    authorize! :create, @seat

    respond_to do |format|
      if @seat.save
        format.html { redirect_to @seat.invitation, notice: 'Seat was successfully created.' }
        format.json { render :show, status: :created, location: @seat }
      else
        format.html { render :new }
        format.json { render json: @seat.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /seats/1
  # PATCH/PUT /seats/1.json
  def update
    authorize! :update, @seat
    respond_to do |format|
      if @seat.update(seat_params)
        format.html { redirect_to @seat.invitation, notice: 'Seat was successfully updated.' }
        format.json { render :show, status: :ok, location: @seat }
      else
        format.html { render :edit }
        format.json { render json: @seat.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /seats/1
  # DELETE /seats/1.json
  def destroy
    authorize! :destroy, @seat
    @seat.destroy
    respond_to do |format|
      format.html { redirect_to @seat.invitation, notice: 'Seat was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_seat
      @seat = Seat.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def seat_params
      params.require(:seat).permit(:docket_id, :relay_id, :pos)
    end
end
