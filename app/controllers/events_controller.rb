class EventsController < ApplicationController
  before_action :set_event, only: [:show, :edit, :update, :destroy, :list, :seed]

  # GET /events
  # GET /events.json
  def index
    authorize! :index, Event
    @events = Event.all
  end

  # GET /events/1
  # GET /events/1.json
  def show
    authorize! :show, @event
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @event }
      format.tex
    end
  end

  # GET /events/new
  def new
    @event = Event.new
    authorize! :new, @event
    @event.competition = Competition.find(params[:competition_id])
    @event.age_min, @event.age_max = 0, 99
    @event.day = 1
    @event.discipline = Discipline.where({
      mode: 'I',
      course: @event.competition.course,
      stroke: 'Freestyle'
    }).first
    @event.stage = "F"
  end

  # GET /events/1/edit
  def edit
    authorize! :edit, @event
  end

  # POST /events
  # POST /events.json
  def create
    @event = Event.new(event_params)
    authorize! :create, @event
    @event.discipline = find_discipline(event_params)

    respond_to do |format|
      if @event.save
        format.html { redirect_to @event, notice: 'Event was successfully created.' }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    authorize! :update, @event
    @event.discipline = find_discipline(event_params)

    respond_to do |format|
      if @event.update(event_params)
        format.html { redirect_to @event.competition, notice: 'Event was successfully updated.' }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    authorize! :destroy, @event
    @event.destroy

    respond_to do |format|
      format.html { redirect_to @event.competition, notice: 'Event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # POST /events/1/list
  def list
    authorize! :list, @event
    @event.list!

    respond_to do |format|
      format.html { redirect_to results_url(event_id: @event) }
    end
  end

  # POST /events/1/seed
  def seed
    authorize! :seed, @event
    @event.seed!(params[:width].to_i, params[:start].to_i)

    respond_to do |format|
      format.html { redirect_to heats_url(event_id: @event) }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:competition_id, :discipline_id, :gender, :pos, :age_min, :age_max, :day, :stage, :seeded_at, :distance, :course, :stroke, :mode)
    end
end
