class SupportsController < ApplicationController
  before_action :set_support, only: [:show, :edit, :update, :destroy]

  # GET /supports
  # GET /supports.json
  def index
    authorize! :index, Support
    @supports = Support.all
  end

  # GET /supports/1
  # GET /supports/1.json
  def show
    authorize! :show, @support
  end

  # GET /supports/new
  def new
    @support = Support.new
    authorize! :new, @support
    @support.user = User.find(params[:user_id])
  end

  # GET /supports/1/edit
  def edit
    authorize! :edit, @support
  end

  # POST /supports
  # POST /supports.json
  def create
    @support = Support.new(support_params)
    authorize! :create, @support

    respond_to do |format|
      if @support.save
        format.html { redirect_to @support.user, notice: 'Support was successfully created.' }
        format.json { render :show, status: :created, location: @support }
      else
        format.html { render :new }
        format.json { render json: @support.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /supports/1
  # PATCH/PUT /supports/1.json
  def update
    authorize! :update, @support
    respond_to do |format|
      if @support.update(support_params)
        format.html { redirect_to @support.user, notice: 'Support was successfully updated.' }
        format.json { render :show, status: :ok, location: @support }
      else
        format.html { render :edit }
        format.json { render json: @support.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /supports/1
  # DELETE /supports/1.json
  def destroy
    authorize! :destroy, @support
    @support.destroy
    respond_to do |format|
      format.html { redirect_to @support.user, notice: 'Support was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_support
      @support = Support.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def support_params
      params.require(:support).permit(:swimmer_id, :user_id)
    end
end
