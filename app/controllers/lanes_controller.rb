class LanesController < ApplicationController

  # GET /lanes
  # GET /lanes.json
  def index
    authorize! :index, Event
    if params[:event_id]
      @event = Event.find(params[:event_id])
      @lanes = @event.lanes
    elsif params[:competition_id]
      @competition = Competition.find(params[:competition_id])
    end
  end

end
