class RelaysController < ApplicationController
  before_action :set_relay, only: [:show, :edit, :update, :destroy]

  # GET /relays
  # GET /relays.json
  def index
    authorize! :index, Relay
    @relays = Relay.all
  end

  # GET /relays/1
  # GET /relays/1.json
  def show
    authorize! :show, @relay
  end

  # GET /relays/new
  def new
    @relay = Relay.new
    authorize! :new, @relay
    @relay.invitation = Invitation.find(params[:invitation_id])
  end

  # GET /relays/1/edit
  def edit
    authorize! :edit, @relay
  end

  # POST /relays
  # POST /relays.json
  def create
    @relay = Relay.new(relay_params)
    authorize! :create, @relay

    respond_to do |format|
      if @relay.save
        format.html { redirect_to @relay.invitation, notice: 'Relay was successfully created.' }
        format.json { render :show, status: :created, location: @relay }
      else
        format.html { render :new }
        format.json { render json: @relay.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /relays/1
  # PATCH/PUT /relays/1.json
  def update
    authorize! :update, @relay
    respond_to do |format|
      if @relay.update(relay_params)
        format.html { redirect_to @relay.invitation, notice: 'Relay was successfully updated.' }
        format.json { render :show, status: :ok, location: @relay }
      else
        format.html { render :edit }
        format.json { render json: @relay.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /relays/1
  # DELETE /relays/1.json
  def destroy
    authorize! :destroy, @relay
    @relay.destroy
    respond_to do |format|
      format.html { redirect_to @relay.invitation, notice: 'Relay was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_relay
      @relay = Relay.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def relay_params
      params.require(:relay).permit(:name, :age_min, :age_max, :invitation_id, :gender)
    end
end
