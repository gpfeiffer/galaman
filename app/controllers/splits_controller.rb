class SplitsController < ApplicationController
  before_action :set_split, only: [:show, :edit, :update, :destroy]

  # GET /splits
  # GET /splits.json
  def index
    authorize! :index, Split
    @splits = Split.all
  end

  # GET /splits/1
  # GET /splits/1.json
  def show
    authorize! :show, @split
  end

  # GET /splits/new
  def new
    @split = Split.new
    authorize! :new, @split
  end

  # GET /splits/1/edit
  def edit
    authorize! :edit, @split
  end

  # POST /splits
  # POST /splits.json
  def create
    @split = Split.new(split_params)
    authorize! :create, @split

    respond_to do |format|
      if @split.save
        format.html { redirect_to @split.result, notice: 'Split was successfully created.' }
        format.json { render :show, status: :created, location: @split }
      else
        format.html { render :new }
        format.json { render json: @split.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /splits/1
  # PATCH/PUT /splits/1.json
  def update
    authorize! :update, @split
    respond_to do |format|
      if @split.update(split_params)
        format.html { redirect_to @split.result, notice: 'Split was successfully updated.' }
        format.json { render :show, status: :ok, location: @split }
      else
        format.html { render :edit }
        format.json { render json: @split.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /splits/1
  # DELETE /splits/1.json
  def destroy
    authorize! :destroy, @split
    @split.destroy
    respond_to do |format|
      format.html { redirect_to @split.result, notice: 'Split was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_split
      @split = Split.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def split_params
      params.require(:split).permit(:result_id, :time, :length)
    end
end
