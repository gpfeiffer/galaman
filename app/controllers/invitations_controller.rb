class InvitationsController < ApplicationController
  before_action :set_invitation, only: [:show, :edit, :update, :destroy]

  # GET /invitations
  # GET /invitations.json
  ##  FIXME: load and authorize @club instead of @invitation?
  def index
    authorize! :index, Invitation
    @invitations = Invitation.all
    @club = Club.find(params[:club_id])
  end

  # GET /invitations/1
  # GET /invitations/1.json
  def show
    authorize! :show, @invitation
    @competition = @invitation.competition
    @invitations = @competition.invitations.includes(:club)
    @events = @invitation.events.includes(:discipline)
    @relays = @invitation.relays
    @dockets = @invitation.dockets.includes(:swimmer, entries: [event: :discipline])
  end

  # GET /invitations/new
  def new
    @invitation = Invitation.new
    authorize! :new, @invitation
#    @invitation.club = Club.find(params[:club_id])
    @invitation.competition = Competition.find(params[:competition_id])
  end

  # GET /invitations/1/edit
  def edit
    authorize! :edit, @invitation
  end

  # POST /invitations
  # POST /invitations.json
  def create
    @invitation = Invitation.new(invitation_params)
    authorize! :create, @invitation

    respond_to do |format|
      if @invitation.save
        format.html { redirect_to @invitation.competition, notice: 'Invitation was successfully created.' }
        format.json { render :show, status: :created, location: @invitation }
      else
        format.html { render :new }
        format.json { render json: @invitation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /invitations/1
  # PATCH/PUT /invitations/1.json
  def update
    authorize! :update, @invitation

    if params[:invitation]
      swimmer_ids = params[:invitation][:swimmer_ids].map(&:to_i)
    else
      swimmer_ids = []
    end
    swimmer_ids = @invitation.swimmer_ids - swimmer_ids
    @invitation.dockets.each do |docket|
      if swimmer_ids.include? docket.swimmer_id
        docket.destroy
      end
    end

    respond_to do |format|
      if @invitation.update(invitation_params)
        format.html { redirect_to @invitation, notice: 'Invitation was successfully updated.' }
        format.json { render :show, status: :ok, location: @invitation }
      else
        format.html { render :edit }
        format.json { render json: @invitation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /invitations/1
  # DELETE /invitations/1.json
  def destroy
    authorize! :destroy, @invitation
    @invitation.destroy
    respond_to do |format|
      format.html { redirect_to @invitation.competition, notice: 'Invitation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_invitation
      @invitation = Invitation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def invitation_params
      params.require(:invitation).permit(:club_id, :competition_id, swimmer_ids: [])
    end
end
