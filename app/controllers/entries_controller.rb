class EntriesController < ApplicationController
  before_action :set_entry, only: [:show, :edit, :update, :destroy]

  # GET /entries
  # GET /entries.json
  def index
    authorize! :index, Entry
    @entries = Entry.all
  end

  # GET /entries/1
  # GET /entries/1.json
  def show
    authorize! :show, @entry

    if @entry.swimmer
      # find personal best and use as default seed time
      swimmer = @entry.swimmer
      discipline = @entry.event.discipline
      @best = swimmer.personal_best(discipline)
      @cobest = swimmer.personal_best(discipline.opposite)
    end
  end

  # GET /entries/new
  def new
    @entry = Entry.new
    authorize! :new, @entry
    @entry.event = Event.find(params[:event_id])
    @entry.subject = params[:subject_type].constantize.find(params[:subject_id])

    if @entry.swimmer
      # find personal best a use as default seed time
      swimmer = @entry.swimmer
      discipline = @entry.event.discipline
      @best = swimmer.personal_best(discipline)
      @cobest = swimmer.personal_best(discipline.opposite)
      if @best
        @entry[:time] = @best.time
      end
    end
  end

  # GET /entries/1/edit
  def edit
    authorize! :edit, @entry
  end

  # POST /entries
  # POST /entries.json
  def create
    @entry = Entry.new(entry_params)
    authorize! :create, @entry
    @entry[:time] = time_from_msc(params[:entry])

    respond_to do |format|
      if @entry.save
        format.html { redirect_to @entry.invitation, notice: 'Entry was successfully created.' }
        format.json { render :show, status: :created, location: @entry }
      else
        format.html { render :new }
        format.json { render json: @entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /entries/1
  # PATCH/PUT /entries/1.json
  def update
    authorize! :update, @entry
    @entry[:time] = time_from_msc(params[:entry])

    respond_to do |format|
      if @entry.update(entry_params)
        format.html { redirect_to @entry.invitation, notice: 'Entry was successfully updated.' }
        format.json { render :show, status: :ok, location: @entry }
      else
        format.html { render :edit }
        format.json { render json: @entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /entries/1
  # DELETE /entries/1.json
  def destroy
    authorize! :destroy, @entry
    @entry.destroy
    respond_to do |format|
      format.html { redirect_to @entry.invitation, notice: 'Entry was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_entry
      @entry = Entry.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def entry_params
      params.require(:entry).permit(:event_id, :subject_id, :subject_type, :time, :lane, :heat, :stage, :group)
    end
end
