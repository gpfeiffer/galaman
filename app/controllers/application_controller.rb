class ApplicationController < ActionController::Base
  before_action :authenticate_user!

  rescue_from CanCan::AccessDenied do |exception|
    flash[:alert] = "Access denied."
    redirect_to root_url
  end

  # how to convert a hash with components mins, secs, cens into time
  def time_from_msc(opts)
    m = opts[:mins].to_i
    s = opts[:secs].to_i
    c = opts[:cens].to_i
    100 * (60 * m + s) + c
  end

  # how to find the discipline, or create a new one
  def find_discipline(opts)
    keys =%w{ distance course stroke mode }
    subs = opts.reject { |k, v| not keys.include?(k) }
    Discipline.where(subs).first || Discipline.create(subs)
  end

end
