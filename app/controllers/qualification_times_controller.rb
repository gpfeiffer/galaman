class QualificationTimesController < ApplicationController
  before_action :set_qualification_time, only: [:show, :edit, :update, :destroy]

  # GET /qualification_times
  # GET /qualification_times.json
  def index
    authorize! :index, QualificationTime
    @qualification_times = QualificationTime.all
  end

  # GET /qualification_times/1
  # GET /qualification_times/1.json
  def show
    authorize! :show, @qualification_time
  end

  # GET /qualification_times/new
  def new
    authorize! :new, QualificationTime
    @qualification_time = QualificationTime.new
    @qualification_time.qualification = Qualification.find(params[:qualification_id])
  end

  # GET /qualification_times/1/edit
  def edit
    authorize! :edit, @qualification_time
  end

  # POST /qualification_times
  # POST /qualification_times.json
  def create
    @qualification_time = QualificationTime.new(qualification_time_params)
    authorize! :create, @qualification_time

    @qualification_time.time = time_from_msc(time_params)
    @qualification_time.discipline = find_discipline(discipline_params)

    respond_to do |format|
      if @qualification_time.save
        format.html { redirect_to @qualification_time.qualification, notice: 'Qualification time was successfully created.' }
        format.json { render :show, status: :created, location: @qualification_time }
      else
        format.html { render :new }
        format.json { render json: @qualification_time.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /qualification_times/1
  # PATCH/PUT /qualification_times/1.json
  def update
    authorize! :update, @qualification_time

    @qualification_time.time = time_from_msc(qualification_time_params)
    @qualification_time.discipline = find_discipline(qualification_time_params)

    respond_to do |format|
      if @qualification_time.update(qualification_time_params)
        format.html { redirect_to @qualification_time.qualification, notice: 'Qualification time was successfully updated.' }
        format.json { render :show, status: :ok, location: @qualification_time }
      else
        format.html { render :edit }
        format.json { render json: @qualification_time.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /qualification_times/1
  # DELETE /qualification_times/1.json
  def destroy
    authorize! :destroy, @qualification_time
    @qualification_time.destroy
    respond_to do |format|
      format.html { redirect_to @qualification_time.qualification, notice: 'Qualification time was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_qualification_time
      @qualification_time = QualificationTime.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def qualification_time_params
      params.require(:qualification_time).permit(:qualification_id, :gender, :age_min, :age_max)
    end

    def time_params
      params.require(:time).permit(:mins, :secs, :cens)
    end

    def discipline_params
      params.require(:discipline).permit(:distance, :course, :stroke, :mode )
    end
end
