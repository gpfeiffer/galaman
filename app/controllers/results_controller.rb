class ResultsController < ApplicationController
  before_action :set_result, only: [:show, :edit, :update, :destroy]

  # GET /results
  # GET /results.json
  def index
    authorize! :index, Result
    if params[:event_id]
      @event = Event.find(params[:event_id])
      @results = @event.results
    elsif params[:invitation_id]
      @invitation = Invitation.find(params[:invitation_id])
    elsif params[:competition_id]
      @competition = Competition.find(params[:competition_id])
    elsif params[:club_id]
      @club = Club.find(params[:club_id])
      @results = @club.results.includes(entry: [:discipline, { event: [:discipline, :competition] }, { subject: :swimmer }])
    else
      @results = Result.all
    end
  end

  # GET /results/1
  # GET /results/1.json
  def show
    authorize! :show, @result
  end

  # GET /results/new
  def new
    @result = Result.new
    authorize! :new, @result
    @result.entry = Entry.find(params[:entry_id])
  end

  # GET /results/1/edit
  def edit
    authorize! :edit, @result
  end

  # POST /results
  # POST /results.json
  def create
    @result = Result.new(result_params)
    authorize! :create, @result
    if params[:result][:comment].blank?
      @result[:time] = time_from_msc(params[:result])
    else
      @result[:time] = 0
      @result[:place] = ""
    end

    respond_to do |format|
      if @result.save
        format.html { redirect_to @result.event, notice: 'Result was successfully created.' }
        format.json { render :show, status: :created, location: @result }
      else
        format.html { render :new }
        format.json { render json: @result.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /results/1
  # PATCH/PUT /results/1.json
  def update
    authorize! :update, @result
    if params[:result][:comment].blank?
      @result[:time] = time_from_msc(params[:result])
    else
      @result[:time] = 0
      params[:result][:place] = ""
    end

    respond_to do |format|
      if @result.update(result_params)
        format.html { redirect_to @result.event, notice: 'Result was successfully updated.' }
        format.json { render :show, status: :ok, location: @result }
      else
        format.html { render :edit }
        format.json { render json: @result.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /results/1
  # DELETE /results/1.json
  def destroy
    authorize! :destroy, @result
    @result.destroy

    respond_to do |format|
      format.html { redirect_to @result.event, notice: 'Result was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_result
      @result = Result.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def result_params
      params.require(:result).permit(:entry_id, :time, :comment, :place, :heat, :lane, :stage)
    end
end
