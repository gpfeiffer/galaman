json.extract! stroke, :id, :name, :short, :code, :created_at, :updated_at
json.url stroke_url(stroke, format: :json)
