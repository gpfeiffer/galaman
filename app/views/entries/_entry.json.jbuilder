json.extract! entry, :id, :event_id, :subject_id, :subject_type, :time, :lane, :heat, :stage, :group, :created_at, :updated_at
json.url entry_url(entry, format: :json)
