json.extract! competition, :id, :name, :location, :course, :age_up, :date, :length, :source, :source_url, :source_date, :created_at, :updated_at
json.url competition_url(competition, format: :json)
