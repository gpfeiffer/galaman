json.extract! result, :id, :entry_id, :time, :comment, :place, :heat, :lane, :stage, :created_at, :updated_at
json.url result_url(result, format: :json)
