json.extract! event, :id, :competition_id, :discipline_id, :gender, :pos, :age_min, :age_max, :day, :stage, :seeded_at, :created_at, :updated_at
json.url event_url(event, format: :json)
