json.extract! split, :id, :result_id, :time, :length, :created_at, :updated_at
json.url split_url(split, format: :json)
