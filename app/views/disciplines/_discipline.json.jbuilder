json.extract! discipline, :id, :gender, :distance, :course, :stroke, :mode, :differential, :created_at, :updated_at
json.url discipline_url(discipline, format: :json)
