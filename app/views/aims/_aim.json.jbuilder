json.extract! aim, :id, :qualification_id, :swimmer_id, :date, :created_at, :updated_at
json.url aim_url(aim, format: :json)
