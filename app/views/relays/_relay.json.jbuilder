json.extract! relay, :id, :name, :age_min, :age_max, :invitation_id, :gender, :created_at, :updated_at
json.url relay_url(relay, format: :json)
