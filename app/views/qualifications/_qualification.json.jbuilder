json.extract! qualification, :id, :name, :description, :short, :source, :source_url, :source_date, :created_at, :updated_at
json.url qualification_url(qualification, format: :json)
