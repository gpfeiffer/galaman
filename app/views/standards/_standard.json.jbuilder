json.extract! standard, :id, :competition_id, :qualification_id, :created_at, :updated_at
json.url standard_url(standard, format: :json)
