json.extract! docket, :id, :invitation_id, :swimmer_id, :age, :created_at, :updated_at
json.url docket_url(docket, format: :json)
