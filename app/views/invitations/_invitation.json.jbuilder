json.extract! invitation, :id, :club_id, :competition_id, :created_at, :updated_at
json.url invitation_url(invitation, format: :json)
