json.extract! qualification_time, :id, :qualification_id, :discipline_id, :gender, :age_min, :age_max, :time, :created_at, :updated_at
json.url qualification_time_url(qualification_time, format: :json)
