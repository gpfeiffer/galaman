json.extract! seat, :id, :docket_id, :relay_id, :pos, :created_at, :updated_at
json.url seat_url(seat, format: :json)
