json.extract! swimmer, :id, :club_id, :user_id, :first, :last, :birthday, :gender, :number, :comment, :created_at, :updated_at
json.url swimmer_url(swimmer, format: :json)
