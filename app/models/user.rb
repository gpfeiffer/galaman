class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :trackable

  has_many :assignments, dependent: :destroy
    has_many :roles, through: :assignments
  has_many :supports, dependent: :destroy
    has_many :beneficiaries, through: :supports, source: :swimmer
  has_one :swimmer

  default_scope { order(:email) }

  def to_s
    name.present? ? name : email.split("@")[0]
  end

  def role? symbol
    roles.map(&:name).include? symbol.to_s.camelcase
  end

  def admin?
    role? :admin
  end

end
