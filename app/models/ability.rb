class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)

    if user.admin?
      can :manage, :all
    else
      can :index, Club
      can :read, [Competition, Discipline, Qualification]
      can :show, User do |u|
        u == user
      end

    end

    if user.role? :parent
      can :read, [Swimmer, Club, Docket, Entry, Result, Event, Invitation, Competition]
      can :manage, Aim, { swimmer: { id: user.beneficiary_ids } }
    end

    if user.role? :swimmer
      can :read, [Swimmer, Club, Docket, Entry, Result, Event, Invitation, Competition]
      can :manage, Aim, { swimmer: user.swimmer }
    end

    if user.role? :coach
      can :read, [Swimmer, Club, Docket, Entry, Result, Event, Invitation, Competition]
#      can :manage, Result
      can [:create, :update], Swimmer
#      can :manage, Aim do |aim|
#        aim.swimmer.supporters.include? user
#      end
      can :manage, Aim
    end

  end
end
