class Split < ApplicationRecord
  belongs_to :result

  attr_accessor :lap

  default_scope { order(:length) }
end
