class Seat < ApplicationRecord
  belongs_to :docket
    has_one :swimmer, through: :docket
  belongs_to :relay
    has_one :invitation, through: :relay
end
