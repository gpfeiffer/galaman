# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_10_23_162845) do

  create_table "aims", force: :cascade do |t|
    t.integer "qualification_id"
    t.integer "swimmer_id"
    t.date "date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "valid_from"
    t.index ["qualification_id"], name: "index_aims_on_qualification_id"
    t.index ["swimmer_id"], name: "index_aims_on_swimmer_id"
  end

  create_table "assignments", force: :cascade do |t|
    t.integer "role_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["role_id"], name: "index_assignments_on_role_id"
    t.index ["user_id"], name: "index_assignments_on_user_id"
  end

  create_table "clubs", force: :cascade do |t|
    t.string "full_name"
    t.string "code"
    t.text "contact"
    t.string "email"
    t.string "lsc", default: ""
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "competitions", force: :cascade do |t|
    t.string "name"
    t.string "location"
    t.string "course"
    t.date "age_up"
    t.date "date"
    t.integer "length"
    t.string "source"
    t.string "source_url"
    t.date "source_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "disciplines", force: :cascade do |t|
    t.string "gender"
    t.integer "distance"
    t.string "course"
    t.string "stroke"
    t.string "mode"
    t.integer "differential"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "dockets", force: :cascade do |t|
    t.integer "invitation_id"
    t.integer "swimmer_id"
    t.integer "age"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["invitation_id"], name: "index_dockets_on_invitation_id"
    t.index ["swimmer_id"], name: "index_dockets_on_swimmer_id"
  end

  create_table "entries", force: :cascade do |t|
    t.integer "event_id"
    t.string "subject_type"
    t.integer "subject_id"
    t.integer "time"
    t.integer "lane"
    t.integer "heat"
    t.string "stage"
    t.integer "group", default: 99
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id"], name: "index_entries_on_event_id"
    t.index ["subject_type", "subject_id"], name: "index_entries_on_subject_type_and_subject_id"
  end

  create_table "events", force: :cascade do |t|
    t.integer "competition_id"
    t.integer "discipline_id"
    t.string "gender"
    t.integer "pos"
    t.integer "age_min"
    t.integer "age_max"
    t.integer "day"
    t.string "stage"
    t.datetime "seeded_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["competition_id"], name: "index_events_on_competition_id"
    t.index ["discipline_id"], name: "index_events_on_discipline_id"
  end

  create_table "invitations", force: :cascade do |t|
    t.integer "club_id"
    t.integer "competition_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["club_id"], name: "index_invitations_on_club_id"
    t.index ["competition_id"], name: "index_invitations_on_competition_id"
  end

  create_table "qualification_times", force: :cascade do |t|
    t.integer "qualification_id"
    t.integer "discipline_id"
    t.string "gender"
    t.integer "age_min"
    t.integer "age_max"
    t.integer "time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["discipline_id"], name: "index_qualification_times_on_discipline_id"
    t.index ["qualification_id"], name: "index_qualification_times_on_qualification_id"
  end

  create_table "qualifications", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.string "short"
    t.string "source"
    t.string "source_url"
    t.date "source_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "relays", force: :cascade do |t|
    t.string "name"
    t.integer "age_min"
    t.integer "age_max"
    t.integer "invitation_id"
    t.string "gender"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["invitation_id"], name: "index_relays_on_invitation_id"
  end

  create_table "results", force: :cascade do |t|
    t.integer "entry_id"
    t.integer "time"
    t.string "comment"
    t.integer "place"
    t.integer "heat"
    t.integer "lane"
    t.string "stage"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["entry_id"], name: "index_results_on_entry_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "seats", force: :cascade do |t|
    t.integer "docket_id"
    t.integer "relay_id"
    t.integer "pos"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["docket_id"], name: "index_seats_on_docket_id"
    t.index ["relay_id"], name: "index_seats_on_relay_id"
  end

  create_table "splits", force: :cascade do |t|
    t.integer "result_id"
    t.integer "time"
    t.integer "length"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["result_id"], name: "index_splits_on_result_id"
  end

  create_table "standards", force: :cascade do |t|
    t.integer "competition_id"
    t.integer "qualification_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["competition_id"], name: "index_standards_on_competition_id"
    t.index ["qualification_id"], name: "index_standards_on_qualification_id"
  end

  create_table "strokes", force: :cascade do |t|
    t.string "name"
    t.string "short"
    t.integer "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "supports", force: :cascade do |t|
    t.integer "swimmer_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["swimmer_id"], name: "index_supports_on_swimmer_id"
    t.index ["user_id"], name: "index_supports_on_user_id"
  end

  create_table "swimmers", force: :cascade do |t|
    t.integer "club_id"
    t.integer "user_id"
    t.string "first"
    t.string "last"
    t.date "birthday"
    t.string "gender"
    t.string "number"
    t.text "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["club_id"], name: "index_swimmers_on_club_id"
    t.index ["user_id"], name: "index_swimmers_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "name", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
