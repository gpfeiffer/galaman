class CreateAims < ActiveRecord::Migration[5.2]
  def change
    create_table :aims do |t|
      t.references :qualification, foreign_key: true
      t.references :swimmer, foreign_key: true
      t.date :date

      t.timestamps
    end
  end
end
