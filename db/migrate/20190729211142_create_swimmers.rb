class CreateSwimmers < ActiveRecord::Migration[5.2]
  def change
    create_table :swimmers do |t|
      t.references :club, foreign_key: true
      t.references :user, foreign_key: true
      t.string :first
      t.string :last
      t.date :birthday
      t.string :gender
      t.string :number
      t.text :comment

      t.timestamps
    end
  end
end
