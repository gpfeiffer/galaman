class CreateStandards < ActiveRecord::Migration[5.2]
  def change
    create_table :standards do |t|
      t.references :competition, foreign_key: true
      t.references :qualification, foreign_key: true

      t.timestamps
    end
  end
end
