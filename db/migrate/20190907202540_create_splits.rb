class CreateSplits < ActiveRecord::Migration[5.2]
  def change
    create_table :splits do |t|
      t.references :result, foreign_key: true
      t.integer :time
      t.integer :length

      t.timestamps
    end
  end
end
