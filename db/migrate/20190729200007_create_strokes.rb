class CreateStrokes < ActiveRecord::Migration[5.2]
  def change
    create_table :strokes do |t|
      t.string :name
      t.string :short
      t.integer :code

      t.timestamps
    end
  end
end
