class CreateQualificationTimes < ActiveRecord::Migration[5.2]
  def change
    create_table :qualification_times do |t|
      t.references :qualification, foreign_key: true
      t.references :discipline, foreign_key: true
      t.string :gender
      t.integer :age_min
      t.integer :age_max
      t.integer :time

      t.timestamps
    end
  end
end
