class CreateQualifications < ActiveRecord::Migration[5.2]
  def change
    create_table :qualifications do |t|
      t.string :name
      t.text :description
      t.string :short
      t.string :source
      t.string :source_url
      t.date :source_date

      t.timestamps
    end
  end
end
