class CreateDisciplines < ActiveRecord::Migration[5.2]
  def change
    create_table :disciplines do |t|
      t.string :gender
      t.integer :distance
      t.string :course
      t.string :stroke
      t.string :mode
      t.integer :differential

      t.timestamps
    end
  end
end
