class CreateEntries < ActiveRecord::Migration[5.2]
  def change
    create_table :entries do |t|
      t.references :event, foreign_key: true
      t.references :subject, polymorphic: true
      t.integer :time
      t.integer :lane
      t.integer :heat
      t.string :stage
      t.integer :group, default: 99

      t.timestamps
    end
  end
end
