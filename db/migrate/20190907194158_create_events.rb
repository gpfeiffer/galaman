class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.references :competition, foreign_key: true
      t.references :discipline, foreign_key: true
      t.string :gender
      t.integer :pos
      t.integer :age_min
      t.integer :age_max
      t.integer :day
      t.string :stage
      t.timestamp :seeded_at

      t.timestamps
    end
  end
end
