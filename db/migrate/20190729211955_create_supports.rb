class CreateSupports < ActiveRecord::Migration[5.2]
  def change
    create_table :supports do |t|
      t.references :swimmer, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
