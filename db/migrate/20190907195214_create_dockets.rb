class CreateDockets < ActiveRecord::Migration[5.2]
  def change
    create_table :dockets do |t|
      t.references :invitation, foreign_key: true
      t.references :swimmer, foreign_key: true
      t.integer :age

      t.timestamps
    end
  end
end
