class CreateSeats < ActiveRecord::Migration[5.2]
  def change
    create_table :seats do |t|
      t.references :docket, foreign_key: true
      t.references :relay, foreign_key: true
      t.integer :pos

      t.timestamps
    end
  end
end
