class CreateCompetitions < ActiveRecord::Migration[5.2]
  def change
    create_table :competitions do |t|
      t.string :name
      t.string :location
      t.string :course
      t.date :age_up
      t.date :date
      t.integer :length
      t.string :source
      t.string :source_url
      t.date :source_date

      t.timestamps
    end
  end
end
