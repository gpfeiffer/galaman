class AddValidFromToAim < ActiveRecord::Migration[5.2]
  def change
    add_column :aims, :valid_from, :date
  end
end
