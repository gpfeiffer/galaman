class CreateResults < ActiveRecord::Migration[5.2]
  def change
    create_table :results do |t|
      t.references :entry, foreign_key: true
      t.integer :time
      t.string :comment
      t.integer :place
      t.integer :heat
      t.integer :lane
      t.string :stage

      t.timestamps
    end
  end
end
