class CreateRelays < ActiveRecord::Migration[5.2]
  def change
    create_table :relays do |t|
      t.string :name
      t.integer :age_min
      t.integer :age_max
      t.references :invitation, foreign_key: true
      t.string :gender

      t.timestamps
    end
  end
end
