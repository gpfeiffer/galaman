class CreateClubs < ActiveRecord::Migration[5.2]
  def change
    create_table :clubs do |t|
      t.string :full_name
      t.string :code
      t.text :contact
      t.string :email
      t.string :lsc, default: ""

      t.timestamps
    end
  end
end
